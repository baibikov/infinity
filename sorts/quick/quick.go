package main

func Quick(values []int, l, r int) {
	if l < r && len(values) > 0 {
		p := partition(values, l, r)
		Quick(values, l, p-1)
		Quick(values, p+1, r)
	}
}

func partition(values []int, l, r int) int {
	less := l

	for i := l; i < r; i++ {
		if values[i] <= values[r] {
			values[i], values[less] = values[less], values[i]
			less++
		}
	}
	values[less], values[r] = values[r], values[less]
	return less
}
