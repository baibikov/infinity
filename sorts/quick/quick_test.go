package main

import (
	"reflect"
	"testing"
)

func TestQuick(t *testing.T) {
	tests := []struct {
		name string
		args []int
		want []int
	}{
		{
			name: "test_1",
			args: []int{0, 31, 122, 42, 543, 11, 23, 53},
			want: []int{0, 11, 23, 31, 42, 53, 122, 543},
		},
		{
			name: "test_2",
			args: []int{562, 32, 53, 565},
			want: []int{32, 53, 562, 565},
		},
		{
			name: "test_3",
			args: []int{115, 6, 10, 21},
			want: []int{6, 10, 21, 115},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Quick(tt.args, 0, len(tt.args)-1)
			if ok := reflect.DeepEqual(tt.args, tt.want); !ok {
				t.Errorf("actual: %v not equal want %v", tt.args, tt.want)
			}
		})
	}
}
